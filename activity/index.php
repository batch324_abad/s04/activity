<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity S04</title>
</head>
<body>
	<h1>Building</h1>
	<p><?php echo $building->getName(); ?></p>
	<p><?php echo $building->getFloors(); ?></p>
	<p><?php echo $building->getAddress(); ?></p>

	<p><?php echo $building->changeName(); ?></p>
	<p><?php $building->setName('Caswyn Complex'); ?></p>


	<h1>Condominium</h1>
	<p><?php echo $condominium->getName(); ?></p>
	<p><?php echo $condominium->getFloors(); ?></p>
	<p><?php echo $condominium->getAddress(); ?></p>

	<p><?php echo $condominium->changeName(); ?></p>
	<p><?php $condominium->setName('Enzo Tower'); ?></p>
</body>
</html>