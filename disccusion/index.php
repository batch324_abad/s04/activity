<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S04 Access Modifiers and Encapsulation</title>
	</head>
	<body>
		<!-- <h1><?php echo $hello; ?></h1> -->

		<h1>Access Modifiers</h1>
		<p><?php //$building->name = 'Caswyn Building'; ?></p>
		<p><?php var_dump($building); ?></p>
		<p><?php echo $building->getName(); ?></p>

		<!-- setter -->
		<p><?php $building->setName('Caswyn Building'); ?>
		</p>
		<!-- getter -->
		<p><?php echo $building->getName(); ?></p>

		<p><?php echo $building->getFloors(); ?></p>

		<!-- protected -->
		<p><?php var_dump($kopiko); ?></p>

		<p><?php echo $milk->getName(); ?></p>
		<?php $milk->setName('Bear Brand'); ?>
		<p><?php echo $milk->getName(); ?></p>

		<p><?php echo $condominium->getName(); ?></p>
		<p><?php var_dump($condominium); ?></p>

	</body>
</html>